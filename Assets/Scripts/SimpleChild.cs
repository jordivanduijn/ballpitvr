using Unity.Entities;
using Unity.Mathematics;

/*
    A non-hierarchical way of handeling parenting.
    The previousRotation and previousTranslation are used to calculate velocity
    when these childs are 'released' from their parent.
*/
public struct SimpleChild : IComponentData {
    public Entity parent;
    public quaternion previousRotation;
    public float3 previousTranslation;
}