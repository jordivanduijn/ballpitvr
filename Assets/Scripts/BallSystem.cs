﻿using Unity.Entities;
using Unity.Transforms;

public class BallSystem : SystemBase {

    protected override void OnUpdate() {

        float deltaTime = Time.DeltaTime;
        
        // a simple timer to keep track of how long a ball has been released
        Entities.ForEach((ref Ball ball, ref Translation translation) => {
            ball.releaseTime += deltaTime;
        }).ScheduleParallel();
    }
}