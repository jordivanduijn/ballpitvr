﻿using Unity.Entities;
using Unity.Mathematics;

public struct BallSpawner : IComponentData {
    public Entity red, green, blue, yellow;
    public float3 spawnLocation;
    public float secondsToSpawn;
    public float secondsBetweenSpawn;
    public int numBallsSpawned;
    public int maxNumBalls;
}