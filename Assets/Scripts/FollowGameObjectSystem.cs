﻿using Unity.Entities;
using Unity.Transforms;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;

public class FollowGameObjectSystem : SystemBase
{
    protected override void OnUpdate()
    {
        Entities.WithoutBurst().ForEach((Entity e, ref FollowGameObject f, ref Translation translate, ref Rotation rotation) =>
        {    
            Transform t = EntityTransformMapper.Instance.GetParentTransform(e);
            translate.Value = new float3(t.position.x, t.position.y, t.position.z);
            rotation.Value  = new quaternion(t.rotation.x, t.rotation.y, t.rotation.z, t.rotation.w);
        }).Run();
    }
}
