using Unity.Entities;

public struct Ball : IComponentData {
    public BallType ballType;
    public float releaseTime;
}