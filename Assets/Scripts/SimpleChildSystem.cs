using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Physics;

public class SimpleChildSystem : SystemBase {
    protected override void OnUpdate()
    {
        var translationEntities = GetComponentDataFromEntity<Translation>();
        var rotationEntities = GetComponentDataFromEntity<Rotation>();

        // every Entity that has a SimpleChild component has its translation and rotation set to that of its parent
        // note that these entities also have a PhysicsVelocity component so they still interact with other physics bodies
        // in a more realistic way and still trigger triggers
        Entities.WithoutBurst().ForEach((ref SimpleChild child, ref Translation translation, ref Rotation rotation, ref PhysicsVelocity velocity) =>
        {  
            child.previousTranslation = translation.Value;
            child.previousRotation    = rotation.Value;

            translation.Value = translationEntities[child.parent].Value;
            rotation.Value    = rotationEntities[child.parent].Value;

            // set the Linear velocity to 0 so the physics system won't interfere with the translation
            velocity.Linear = new float3(0);
        }).Run();
    }
}