﻿using UnityEngine;
using Unity.Entities;

public class FollowGameObjectAuthoring : MonoBehaviour, IConvertGameObjectToEntity {

    // the GameObject Transform that this Entity needs to be 'parented' to.
    public Transform parentTransform;

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
        // add this Entity and its Transform to the EntityTransformMapper Dictionary,
        // so it can be used in the 'FollowGameObjectSystem'.
        EntityTransformMapper.Instance.SetParentTransform(entity, parentTransform);
        dstManager.AddComponentData(entity, new FollowGameObject {  });
    }
}
