﻿using Unity.Entities;

/*
    This component is used by Entities that are grabbed.
    It is empty now, but extra useful data could be stored in here.
*/

public struct Grabbed : IComponentData { }