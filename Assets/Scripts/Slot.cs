﻿using Unity.Entities;

public struct Slot : IComponentData {
    // the type (color) of the balls that 'fit' into this slot
    public BallType ballType;

    // the actual Entity that is slotted in this slot
    public Entity ballEntity;
}