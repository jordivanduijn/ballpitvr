﻿using UnityEngine;

public class BeltPositioner : MonoBehaviour {

    [SerializeField] private Transform head;

    // keep the belt directly 30cm below the head. A more sophisticated way of positioning the belt could be used here.
    void Update() {
        transform.position = new Vector3(head.position.x, head.position.y - 0.3f, head.position.z);
    }
}