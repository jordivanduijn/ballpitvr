﻿using Unity.Entities;

/*
    (empty) component data used by the 'FollowGameObjectSystem' to loop
    over every Entity that needs to be 'parented' to a GameObject Transform.
    If there needs to be any kind of translation/rotation/scale offset,
    it could be stored in this component.
*/
public struct FollowGameObject : IComponentData { }