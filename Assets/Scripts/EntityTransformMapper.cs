﻿using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

/* 
    This (singleton) class keeps track of a dictionary that maps
    an Entity to a MonoBehaviour Transform.  In the 'FollowGameObjectSystem',
    this dictionary is used to 'parent' the Entity to its corresponding
    GameObject Transform.
*/
public class EntityTransformMapper : MonoBehaviour
{
    public static EntityTransformMapper Instance { get; private set; }

    private Dictionary<Entity, Transform> transforms = new Dictionary<Entity, Transform>();

    private void Awake() {
        Instance = this;
    }

    public Transform GetParentTransform(Entity e){
        return transforms[e];
    }

    public void SetParentTransform(Entity e, Transform t){
        transforms[e] = t;
    }
}