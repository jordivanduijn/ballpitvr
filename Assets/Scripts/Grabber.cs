﻿using Unity.Entities;

public struct Grabber : IComponentData {
    public Entity grabbable;
    public HandType handType;
}