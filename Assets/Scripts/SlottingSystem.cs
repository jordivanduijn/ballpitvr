﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;
using Unity.Mathematics;

public class SlottingSystem : JobComponentSystem {

    public static float closestSlotDistance = float.MaxValue;
    static Entity closestBall, closestSlot;
    
    private struct SlottingJob : ITriggerEventsJob {

        public ComponentDataFromEntity<Slot> slotEntities;
        public ComponentDataFromEntity<Ball> ballEntities;
        public ComponentDataFromEntity<Translation> translationEntities;
        public ComponentDataFromEntity<SimpleChild> simpleChildEntities;
        public EntityCommandBuffer entityCommandBuffer;

        public void Execute(TriggerEvent triggerEvent) {
            
            Entity slotEntity, ballEntity;

            // check which Entity is the ball and which is the slot
            if(slotEntities.HasComponent(triggerEvent.Entities.EntityA) && ballEntities.HasComponent(triggerEvent.Entities.EntityB)) {
                slotEntity = triggerEvent.Entities.EntityA;
                ballEntity = triggerEvent.Entities.EntityB;
            } else if(slotEntities.HasComponent(triggerEvent.Entities.EntityB) && ballEntities.HasComponent(triggerEvent.Entities.EntityA)) {
                slotEntity   = triggerEvent.Entities.EntityB;
                ballEntity = triggerEvent.Entities.EntityA;
            } else {
                return;
            }

            Slot slot = slotEntities[slotEntity];
            Ball ball = ballEntities[ballEntity];
            
            // only put the ball in this slot if certain conditions apply
            if(simpleChildEntities.HasComponent(ballEntity) // is this ball already a child of something else?
            || slot.ballEntity != Entity.Null               // does this slot already have a child?
            || slot.ballType != ball.ballType               // do the slot type and the balltype match?
            || ball.releaseTime > 0.1f) return;             // has the ball been released for more than 0.1 seconds?

            // just like with the GrabSystem, there could be multiple slots overlapping, so keep track of the closest slot
            float slotDistance = math.length(translationEntities[slotEntity].Value - translationEntities[ballEntity].Value);
            if(slotDistance >= closestSlotDistance) return;

            closestSlotDistance = slotDistance;
            closestSlot = slotEntity;
            closestBall = ballEntity;
        }
    }
    
    private BuildPhysicsWorld buildPhysicsWorld;
    private StepPhysicsWorld stepPhysicsWorld;
    private EndSimulationEntityCommandBufferSystem entityCommandBufferSystem;

    protected override void OnCreate() {
        base.OnCreate();

        buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
        stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
        entityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps) {

        var slotEntities = GetComponentDataFromEntity<Slot>();

        // do the actual slotting here
        if(closestSlot != Entity.Null) {
            Slot slotData = slotEntities[closestSlot];
            slotData.ballEntity = closestBall;
            EntityManager.SetComponentData(closestSlot, slotData);
            EntityManager.AddComponentData(closestBall, new SimpleChild { parent = closestSlot });
            closestSlot = Entity.Null;
            closestSlotDistance = float.MaxValue;
        }
        
        // setup the parameters for the SlottingJob
        SlottingJob slottingJob = new SlottingJob {
            slotEntities         = slotEntities,
            ballEntities         = GetComponentDataFromEntity<Ball>(),
            translationEntities  = GetComponentDataFromEntity<Translation>(),
            simpleChildEntities  = GetComponentDataFromEntity<SimpleChild>(),
            entityCommandBuffer  = entityCommandBufferSystem.CreateCommandBuffer()
        };

        JobHandle jobHandle = slottingJob.Schedule(stepPhysicsWorld.Simulation, ref buildPhysicsWorld.PhysicsWorld, inputDeps);

        return jobHandle;
    }
}