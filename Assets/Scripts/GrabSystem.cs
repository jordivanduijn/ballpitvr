﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;
using Unity.Mathematics;

public class GrabSystem : JobComponentSystem {

    /*
        During a GrabJob cycle, a hand could collide with multiple balls.
        In this case, the ball closest to the hand should be picked up.
        The following static variables are used to keep track of the closest ball,
        so it can be picked up later.
    */
    public static float lGrabDistance = float.MaxValue, rGrabDistance = float.MaxValue;
    static Entity closestGrabbableLeft, closestGrabberLeft;
    static Entity closestGrabbableRight, closestGrabberRight;
    
    private struct GrabJob : ITriggerEventsJob {

        public ComponentDataFromEntity<Grabber> grabberEntities;
        public ComponentDataFromEntity<Translation> translationEntities;
        public ComponentDataFromEntity<Rotation> rotationEntities;
        public bool lTriggerDown, rTriggerDown;

        public void Execute(TriggerEvent triggerEvent) {
            
            Entity grabber, grabbable;
            
            // figure out which Entity is the grabber and which is the grabbable
            if(grabberEntities.HasComponent(triggerEvent.Entities.EntityA)) {
                grabber   = triggerEvent.Entities.EntityA;
                grabbable = triggerEvent.Entities.EntityB;
            } else if(grabberEntities.HasComponent(triggerEvent.Entities.EntityB)) {
                grabber   = triggerEvent.Entities.EntityB;
                grabbable = triggerEvent.Entities.EntityA;
            } else {
                return;
            }
            
            Grabber g = grabberEntities[grabber];

            // check if the correct trigger is being pressed            
            if( (g.handType == HandType.Left  && !lTriggerDown) 
             || (g.handType == HandType.Right && !rTriggerDown) ) return;
             
            // calculate the grab distance in case of multiple collisions so only the closest Entity will be picked up
            float grabDistance =  math.length(translationEntities[grabbable].Value - translationEntities[grabber].Value);
            
            // if a closer one has been found, save it so it can actually be grabbed when the complete loop has been finished
            if(g.handType == HandType.Left) {
                if(grabDistance >= GrabSystem.lGrabDistance) return;
                lGrabDistance = grabDistance;
                closestGrabbableLeft = grabbable;
                closestGrabberLeft   = grabber;
            }

            else if(g.handType == HandType.Right) {
                if(grabDistance >= GrabSystem.rGrabDistance) return;
                rGrabDistance = grabDistance;
                closestGrabbableRight = grabbable;
                closestGrabberRight   = grabber;
            }

        }
    }
    
    private BuildPhysicsWorld buildPhysicsWorld;
    private StepPhysicsWorld stepPhysicsWorld;

    protected override void OnCreate() {
        base.OnCreate();

        buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
        stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps) { 

        // this is where the actual 'grabbing' takes place.
        if(closestGrabbableLeft != Entity.Null) {
            EntityManager.AddComponentData(closestGrabbableLeft, new SimpleChild { parent = closestGrabberLeft });
            EntityManager.AddComponentData(closestGrabbableLeft, new Grabbed { });
            closestGrabbableLeft = Entity.Null;
            lGrabDistance = float.MaxValue;
        }

        if(closestGrabbableRight != Entity.Null) {
            EntityManager.AddComponentData(closestGrabbableRight, new SimpleChild { parent = closestGrabberRight });
            EntityManager.AddComponentData(closestGrabbableRight, new Grabbed { });
            closestGrabbableRight = Entity.Null;
            rGrabDistance = float.MaxValue;
        }

        // set up a couple of parameters to be used by the trigger job
        bool lTriggerDown = OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.LTouch)
                         || OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger , OVRInput.Controller.LTouch);
                         
        bool rTriggerDown = OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTouch)
                         || OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger , OVRInput.Controller.RTouch);

        // if no trigger is being pressed at this moment, don't even bother scheduling the grab job           
        if(!(lTriggerDown || rTriggerDown)) return inputDeps;
        
        GrabJob grabJob = new GrabJob {
            grabberEntities      = GetComponentDataFromEntity<Grabber>(),
            translationEntities  = GetComponentDataFromEntity<Translation>(),
            rotationEntities     = GetComponentDataFromEntity<Rotation>(),
            lTriggerDown         = lTriggerDown,
            rTriggerDown         = rTriggerDown
        };

        return grabJob.Schedule(stepPhysicsWorld.Simulation, ref buildPhysicsWorld.PhysicsWorld, inputDeps);
    }
}