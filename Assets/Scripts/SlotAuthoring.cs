﻿using UnityEngine;
using Unity.Entities;

public class SlotAuthoring : MonoBehaviour, IConvertGameObjectToEntity {
    
    // the type (color) of the balls that 'fit' into this slot
    public BallType ballType;

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
        dstManager.AddComponentData(entity, new Slot { ballType = ballType });
    }
}
