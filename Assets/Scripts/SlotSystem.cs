﻿using Unity.Entities;
using Unity.Transforms;

public class SlotSystem : SystemBase {

    protected override void OnUpdate() {

        var simpleChildEntities = GetComponentDataFromEntity<SimpleChild>();

        // let the slot itself manage if it is holding anything or not (by looking at the parent of its child)
        Entities.WithoutBurst().ForEach((Entity slotEntity, ref Slot slot, ref Translation translation) => {
            if(slot.ballEntity != Entity.Null && slotEntity != simpleChildEntities[slot.ballEntity].parent) {                
                slot.ballEntity = Entity.Null;
            }
        }).Run();
    }
}