﻿using UnityEngine;
using Unity.Entities;

public enum HandType { Left, Right }

public class GrabberAuthoring : MonoBehaviour, IConvertGameObjectToEntity {

    public HandType handType;

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
        dstManager.AddComponentData(entity, new Grabber { handType = handType });
    }
}
