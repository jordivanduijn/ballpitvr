﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Physics;

public class BallSpanwerSystem : SystemBase {

    private EndSimulationEntityCommandBufferSystem endSimulationEntityCommandBufferSystem;
    private Random random;

    protected override void OnCreate() {
        endSimulationEntityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        random = new Random(42);
    }

    protected override void OnUpdate() {

        var entityCommandBuffer = endSimulationEntityCommandBufferSystem.CreateCommandBuffer();
        float deltaTime = Time.DeltaTime;

        Entities.WithoutBurst().ForEach((Entity spawnerEntity, ref BallSpawner s) => {

            // only spawn every 'secondsBetweenSpawn' seconds
            s.secondsToSpawn -= deltaTime;
            if(s.secondsToSpawn > 0) return;

            // reset spawn timer
            s.secondsToSpawn = s.secondsBetweenSpawn;

            // pick a random ball color
            int ballIndex = random.NextInt(4);
            Entity                  ballEntityPrefab = s.red;
            if     (ballIndex == 1) ballEntityPrefab = s.green;
            else if(ballIndex == 2) ballEntityPrefab = s.blue;
            else if(ballIndex == 3) ballEntityPrefab = s.yellow;

            // set random horizontal velocity
            float3 randomVelocity = new float3(random.NextFloat(-2,2), 0, random.NextFloat(-2,2));            
            Entity instance = entityCommandBuffer.Instantiate(ballEntityPrefab);
            entityCommandBuffer.SetComponent<Translation>(instance, new Translation { Value = s.spawnLocation });
            entityCommandBuffer.SetComponent<PhysicsVelocity>(instance, new PhysicsVelocity { Linear = randomVelocity });

            // keep track of the number of spawned balls and stop if the maximum has been reached
            s.numBallsSpawned++;
            if(s.numBallsSpawned >= s.maxNumBalls)
                entityCommandBuffer.RemoveComponent<BallSpawner>(spawnerEntity);
        }).Run();
    }
}