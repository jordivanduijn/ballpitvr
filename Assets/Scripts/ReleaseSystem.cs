using Unity.Entities;
using Unity.Transforms;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Mathematics;


public class ReleaseSystem : SystemBase {
    protected override void OnUpdate() {
        
        var entityCommandBuffer = entityCommandBufferSystem.CreateCommandBuffer();
        
        bool lTriggerDown = OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.LTouch)
                         || OVRInput.Get(OVRInput.Button.PrimaryHandTrigger , OVRInput.Controller.LTouch);
                         
        bool rTriggerDown = OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTouch)
                         || OVRInput.Get(OVRInput.Button.PrimaryHandTrigger , OVRInput.Controller.RTouch);

        if(lTriggerDown && rTriggerDown) return;

        float deltaTime = Time.DeltaTime;
        var grabberEntities = GetComponentDataFromEntity<Grabber>();

        Entities.WithoutBurst().ForEach((Entity grabbable, ref Grabbed g, ref SimpleChild child, ref Ball ball, ref Translation translate, ref Rotation rotation) => {
            
            HandType handType = grabberEntities[child.parent].handType;
            if( (handType == HandType.Left && lTriggerDown) || (handType == HandType.Right && rTriggerDown) ) return;

            if(handType == HandType.Left)  GrabSystem.lGrabDistance = float.MaxValue;
            if(handType == HandType.Right) GrabSystem.rGrabDistance = float.MaxValue;

            ball.releaseTime = 0;

            float3 linearVelocity = translate.Value - child.previousTranslation;
            entityCommandBuffer.SetComponent<PhysicsVelocity>(grabbable, new PhysicsVelocity{ Linear = linearVelocity * deltaTime * 7000 });
            entityCommandBuffer.RemoveComponent<SimpleChild>(grabbable);
            entityCommandBuffer.RemoveComponent<Grabbed>(grabbable);
        }).Run();
    }

    private BuildPhysicsWorld buildPhysicsWorld;
    private StepPhysicsWorld stepPhysicsWorld;
    private EndSimulationEntityCommandBufferSystem entityCommandBufferSystem;

    protected override void OnCreate() {
        base.OnCreate();

        buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
        stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
        entityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }
}