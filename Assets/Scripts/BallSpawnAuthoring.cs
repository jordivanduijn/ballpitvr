using UnityEngine;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;

public class BallSpawnAuthoring : MonoBehaviour, IDeclareReferencedPrefabs, IConvertGameObjectToEntity {

    [SerializeField] private GameObject redBall, greenBall, blueBall, yellowBall;
    [SerializeField] private float secondsBetweenSpawn;
    [SerializeField] private int maxNumBalls;

    public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs) {
        referencedPrefabs.Add(redBall);
        referencedPrefabs.Add(greenBall);
        referencedPrefabs.Add(blueBall);
        referencedPrefabs.Add(yellowBall);
    }

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {

        dstManager.AddComponentData(entity, new BallSpawner { 
            red    = conversionSystem.GetPrimaryEntity(redBall),
            green  = conversionSystem.GetPrimaryEntity(greenBall),
            blue   = conversionSystem.GetPrimaryEntity(blueBall),
            yellow = conversionSystem.GetPrimaryEntity(yellowBall),
            spawnLocation = new float3(transform.position.x, transform.position.y, transform.position.z),
            secondsBetweenSpawn = secondsBetweenSpawn,
            maxNumBalls = maxNumBalls
        });
    }
}
