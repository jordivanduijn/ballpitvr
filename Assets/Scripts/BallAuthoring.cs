﻿using UnityEngine;
using Unity.Entities;

public enum BallType { Red, Green, Blue, Yellow }

public class BallAuthoring : MonoBehaviour, IConvertGameObjectToEntity {

    public BallType ballType;

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
        // set releaseTime to 1 so spawning balls won't shoot into the belt
        dstManager.AddComponentData(entity, new Ball { ballType = ballType, releaseTime = 1 });
    }
}
